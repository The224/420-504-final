package ca.claurendeau.examen504final.State;

public class StateMalheureuse implements State {
    @Override
    public String getWhy() {
        return "J'ai besoin d'un MacBook Pro pour être une personne heureuse";
    }

    @Override
    public String toString() {
        return "malheureuse";
    }
}
