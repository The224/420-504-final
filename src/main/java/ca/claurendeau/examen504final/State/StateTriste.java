package ca.claurendeau.examen504final.State;

public class StateTriste implements State {
    @Override
    public String getWhy() {
        return "Je fais parti des gens qui n'auront jamais de MacBook Pro";
    }

    @Override
    public String toString() {
        return "triste";
    }
}
