package ca.claurendeau.examen504final.State;

public class StateHeureuse implements State {
    @Override
    public String getWhy() {
        return "J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!";
    }

    @Override
    public String toString() {
        return "heureuse";
    }
}
