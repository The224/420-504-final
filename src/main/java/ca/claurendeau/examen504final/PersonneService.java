package ca.claurendeau.examen504final;

import java.util.ArrayList;
import java.util.List;

public class PersonneService {
    
    public static void main(String[] args) {
        
        List<Personne> personnes = new ArrayList<>();
        personnes.add(new Personne(Personne.HEUREUSE));
        personnes.add(new Personne(Personne.MALHEUREUSE));
        personnes.add(new Personne(Personne.TRISTE));
        
        personnes.forEach(System.out::println);
        
        for (Personne personne : personnes) {
            System.out.println(personne.getWhyHumeur());
        }
    }
}
