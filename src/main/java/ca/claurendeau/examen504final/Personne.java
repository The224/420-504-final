package ca.claurendeau.examen504final;

import ca.claurendeau.examen504final.State.State;
import ca.claurendeau.examen504final.State.StateHeureuse;
import ca.claurendeau.examen504final.State.StateMalheureuse;
import ca.claurendeau.examen504final.State.StateTriste;

/**
 * 
 * Remanier le code ci-dessous de façon à ce qu'il utilise le 'State/Strategy Pattern'
 * 
 * Vous devrez nommer chacun des remaniements que vous faites ainsi que de faire un commit à chaque remaniement.
 *
 */
public class Personne {

    public final static String HEUREUSE = "heureuse";
    public final static String MALHEUREUSE = "malheureuse";
    public final static String TRISTE = "triste";

    private State humeur;

    public Personne(String humeur) {
        changeHumeur(humeur);
    }

    public void setHumeur(final String newhumeur) {
        changeHumeur(newhumeur);
    }

    public State getHumeur() {
        return humeur;
    }

    public String getWhyHumeur() {
        return humeur.getWhy();
    }

    @Override
    public String toString() {
        return "Personne [humeur=" + humeur + "]";
    }

    private void changeHumeur(String humeur) {
        if (HEUREUSE.equals(humeur))
            this.humeur = new StateHeureuse();
        else if (MALHEUREUSE.equals(humeur))
            this.humeur = new StateMalheureuse();
        else if (TRISTE.equals(humeur))
            this.humeur = new StateTriste();
        else {
            // throw new NoExistingHumeur();
        }
    }
}
