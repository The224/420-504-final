package ca.claurendeau.examen504final;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class PersonneTest {

    private Personne personne;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        personne = null;
    }

    @Test
    public void testPersonneHeureuse() {
        personne = new Personne(Personne.HEUREUSE);
        assertEquals(Personne.HEUREUSE,personne.getHumeur().toString());
    }
    @Test
    public void testPersonneMalheureuse() {
        personne = new Personne(Personne.MALHEUREUSE);
        assertEquals(Personne.MALHEUREUSE,personne.getHumeur().toString());
    }
    @Test
    public void testPersonneTriste() {
        personne = new Personne(Personne.TRISTE);
        assertEquals(Personne.TRISTE,personne.getHumeur().toString());
    }

    @Test
    public void testPersonneChangementHumeur() {
        personne = new Personne(Personne.TRISTE);
        assertEquals(Personne.TRISTE,personne.getHumeur().toString());

        personne.setHumeur(Personne.MALHEUREUSE);
        assertEquals(Personne.MALHEUREUSE,personne.getHumeur().toString());

        personne.setHumeur(Personne.HEUREUSE);
        assertEquals(Personne.HEUREUSE,personne.getHumeur().toString());
    }

    @Test
    public void testPersonnePrintMsg() {
        personne = new Personne(Personne.MALHEUREUSE);
        assertEquals("J'ai besoin d'un MacBook Pro pour être une personne heureuse",personne.getHumeur().getWhy());
    }


}
